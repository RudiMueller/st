# Simple Terminal

This is a fork of the simple terminal project of [suckless.org](https://suckless.org).

## Original data

The master branch contains the master of the original project.
Read the [original Readme](./README) for basic usage.
My changes are on the fork branch.

## Applied patches

To fit my workflow I've applied some patches.
- [xresoruces](https://st.suckless.org/patches/xresources)
- [alpha focus highlight](https://st.suckless.org/patches/alpha_focus_highlight)
- [keyboard_select](https://st.suckless.org/patches/clipboard)
- [one clipboard](https://st.suckless.org/patches/clipboard)
- [selectioncolors](https://st.suckless.org/patches/selectioncolors)
- [bold is not bright](https://st.suckless.org/patches/bold-is-not-bright)

## Xresources options

This build provides the configuration with Xresources file.

The supported resources are:
- st.St.font
- st.St.color0
- st.St.color1
- st.St.color2
- st.St.color3
- st.St.color4
- st.St.color5
- st.St.color6
- st.St.color7
- st.St.color8
- st.St.color9
- st.St.color10
- st.St.color11
- st.St.color12
- st.St.color13
- st.St.color14
- st.St.color15
- st.St.cursorColor
- st.St.selectionbg
- st.St.selectionfg
- st.St.termname
- st.St.shell
- st.St.xfps
- st.St.actionfps
- st.St.blinktimeout
- st.St.bellvolume
- st.St.tabspaces
- st.St.borderpx
- st.St.cwscale
- st.St.chscale
- st.St.alpha
- st.St.alphaUnfocussed

